## 什么是Storybook
Storybook是用于创建一套独立于框架，业务逻辑的组件库。

### Component-Driven Development CDD
从下至上创建UI，最后以整体界面结尾

### stories.js文件 - CSF (Component Story Format)
在storybook架构中有两种基础级别：组件和组件的子故事。子故事可看作是每个组件的一种变化，我们可以为每个组件创建多个故事。

#### 创建故事的3种方法
1. `storiesOf`的API
2. CSF，即包含故事对象的`stories.js`或`stories.tsx`
3. CSF和MDX混合

在`.stories.js`文件中，通过默认导出(`export default`)一个包含`component, title, excludeStories`3个属性的对象，告诉storybook我们正在处理的组件信息。<br>
+ title：在页面的侧边栏显示的组件名字
+ excludeStories：在story文件中导出，但不应被当成story渲染的组件

```jsx
// xx.stories.js
import React from 'react';
import { action } from '@storybook/addon-actions';

import Task from './Task';

export default {
  component: Task,
  title: 'Task',
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/,
};

export const taskData = { // not exported
  id: '1',
  title: 'Test Task',
  state: 'TASK_INBOX',
  updatedAt: new Date(2018, 0, 1, 9, 0),
};

export const actionsData = { // not exported
  onPinTask: action('onPinTask'),
  onArchiveTask: action('onArchiveTask'),
};
```

#### 定义故事
导出一个函数，故事则是一个返回需要*渲染的组件*的函数，该组件应是接收一系列属性，类似于*无状态函数*组件。

```jsx
// xx.stories.js
export const Default = () => <Task task={{ ...taskData }} {...actionsData} />;

export const Pinned = () => <Task task={{ ...taskData, state: 'TASK_PINNED' }} {...actionsData} />;

export const Archived = () => (
  <Task task={{ ...taskData, state: 'TASK_ARCHIVED' }} {...actionsData} />
);
```

#### actions() 方法
`actions()`可以在组件被点击时创建回调，显示在Storybook的actions panel中。
在创建故事的时候，我们可以用一个基础的组件数据e.g. taskData去创建组件的其他形态，基础数据通常是真实数据的模拟。

## MDX
MDX是Markdown和JSX的混合标准文件格式，是允许描述文档和故事并存在同一文件的语法。

### MDX形式的CSF (Component Story Format)
这样的CSF包含一组称为`Doc Blocks`的组件，这允许storybook将MDX文件转换为故事，使用MDX定义的故事和storybook自身的故事是一样的。

## 坑
遇到不能解析某个包的报错，或找不到对应的模块，解决方法：
1. 删除`node_modules`, `package-lock.json`
2. 清除缓存`npm cache clear --force`
3. 重新`npm install`